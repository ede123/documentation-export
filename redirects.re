^keys.+$|keys/%s
^inkscape-man.+$|man/%s
^(.+/tutorial-[a-z]+)\.html$|tutorials/%s.en.html
^.+/tutorial.+$|tutorials/%s
^man/inkscape-man(\.[A-Za-z_]+)?.html$|man/man/inkscape-man092%s.html
